﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokeAPI;

namespace PokedexGeneratorV2
{
    class Program
    {
        //(width, height)
        static readonly Size CardSize = new Size(250, 500);
        static readonly Rectangle CardBorderRectangle = new Rectangle(0, 0, CardSize.Width, CardSize.Height);
        static readonly Rectangle ImageBorderRectangle = new Rectangle(10, 10, CardSize.Width - 20, CardSize.Width - 20);
        static readonly Rectangle ImageRectangle = new Rectangle(ImageBorderRectangle.X + 2, ImageBorderRectangle.Y + 2, ImageBorderRectangle.Width - 4, ImageBorderRectangle.Height - 4);
        static readonly Point TypePosition = new Point(ImageRectangle.X + ImageRectangle.Width / 2, ImageRectangle.Y + ImageRectangle.Height);
        static readonly Point NamePosition = new Point(5, 3);
        const int PixelEntreLesStats = 25;
        const int LeftStatMargin = 20;
        const int NameY = 255;
        const int StatY = 410;
        const int RectangleStatY = 425;
        const int RectangleStatHeight = 5;
        const int RectangleNameHeight = 500;
        const int RectangleNameWidth = 150;
        static readonly Font statTextFont = new Font("Tahoma", 10);
        static readonly Font nameTextFont = new Font("Tahoma", 15);
        static string StatName;
        static int StatBase;
        static string Stat;
        static SizeF fontSize;
        static RectangleF rectf;
        //static readonly string[] StatConverter = new string[] { "Hp", "Attack", "Defense", "Sp. Atk", "Sp. Def", "Speed" };


        static readonly Dictionary<string, Color> PokemonTypeColors = new Dictionary<string, Color>() { 
            {"flying",      getColorFromRGBString("A890F0") },
            {"bug",         getColorFromRGBString("A8B820") },
            {"dark",        getColorFromRGBString("705848") },
            {"dragon",      getColorFromRGBString("7038F8") },
            {"electric",    getColorFromRGBString("F8D030") },
            {"fairy",       getColorFromRGBString("EE99AC") },
            {"fighting",    getColorFromRGBString("C03028") },
            {"fire",        getColorFromRGBString("F08030") },
            {"ghost",       getColorFromRGBString("705898") },
            {"grass",       getColorFromRGBString("78C850") },
            {"ground",      getColorFromRGBString("E0C068") },
            {"ice",         getColorFromRGBString("98D8D8") },
            {"water",       getColorFromRGBString("6890F0") },
            {"steel",       getColorFromRGBString("B8B8D0") },
            {"rock",        getColorFromRGBString("B8A038") },
            {"psychic",     getColorFromRGBString("F85888") },
            {"poison",      getColorFromRGBString("A040A0") },
            {"normal",      getColorFromRGBString("A8A878") }
        };

        static readonly Dictionary<string, string> StatTextConverter = new Dictionary<string, string>() {
            {"hp",                  "Hp" },
            {"attack",              "Attack" },
            {"defense",             "Defense" },
            {"special-attack",      "Sp. Atk" },
            {"special-defense",     "Sp. Def" },
            {"speed",               "Speed" },
        };

        //Get list des pokémon
        static void Main(string[] args)
        {
            DisplayMenu();
        }

        static void DisplayMenu()
        {
            int choix;
            Console.WriteLine("Bienvenue dans PokeGen, veuillez faire un choix parmi les menus suivants");
            Console.WriteLine("1 - Générer tout les Pokémons");
            Console.WriteLine("2 - Générer des Pokémons en imposant une limite");
            choix = Convert.ToInt32(Console.ReadLine());
            switch (choix)
            {
                case 1:
                    GeneratePokemon(809);
                    break;
                case 2:
                    GeneratePokemonWithLimit();
                    break;
                default:
                    Console.WriteLine("Veuillez respectez les choix proposé, merci");
                    break;
            }
        }

        static void GeneratePokemon(int number)
        {
            int alternate;
            bool wantAlternate = true;
            Console.WriteLine("Générer les formes alternatives ?");
            Console.WriteLine("1 - Oui");
            Console.WriteLine("2 - Non");
            alternate = Convert.ToInt32(Console.ReadLine());
            if (alternate == 2)
                wantAlternate = false;

            var ListPokemonSpecies = DataFetcher.GetResourceList<NamedApiResource<PokemonSpecies>, PokemonSpecies> (number).Result;

            foreach (NamedApiResource<PokemonSpecies> pkmnSpeciesResource in ListPokemonSpecies)
            {
                PokemonSpecies pkmnSpecies = pkmnSpeciesResource.GetObject().Result;
                if (!wantAlternate)
                {
                    if (pkmnSpecies.Varieties[0].IsDefault)
                    {
                        ProcessPokemon(pkmnSpecies, "./export/");
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    ProcessPokemon(pkmnSpecies, "./export/");
                }
            }
        }

        static void GeneratePokemonWithLimit()
        {
            Console.WriteLine("Entrez la limite de Pokémon que vous voulez générer");
            int number = Convert.ToInt32(Console.ReadLine());
            GeneratePokemon(number);
        }

        static void ProcessPokemon(PokemonSpecies pkmnSpecies, string savePath)
        {
            foreach (PokemonSpeciesVariety pkmnSpeciesVariety in pkmnSpecies.Varieties)
            {
                Bitmap pkmnImage = new Bitmap(CardSize.Width, CardSize.Height);
                Graphics canvas = Graphics.FromImage(pkmnImage);

                Pokemon pkmn = pkmnSpeciesVariety.Pokemon.GetObject().Result;


                DrawBackground(pkmnSpecies, pkmn, canvas);
                DrawBorders(pkmnSpecies, pkmn, canvas);
                DrawImage(pkmnSpecies, pkmn, canvas);
                DrawTypes(pkmnSpecies, pkmn, canvas);
                CreatePokemonNameText(pkmnSpecies.Name, canvas);
                CreatePokemonStatText(pkmnSpecies, pkmn, canvas);

                GenerateImageFile(pkmnSpecies, pkmn, pkmnImage, savePath);
            }


        }

        public static void CreatePokemonStatText(PokemonSpecies pkmnSpecies, Pokemon pkmn, Graphics canvas)
        {
            
            for (int i = 0; i < 6; i++)
            {
                StatName = StatTextConverter[pkmn.Stats[i].Stat.Name];
                StatBase = pkmn.Stats[i].BaseValue;
                Stat = StatName + " : " + StatBase.ToString();
                fontSize = MeasureString(Stat, statTextFont, canvas);
                //Placement du texte sur la carte                         -1 nécessaire pour inverser l'ordre d'apparition
                rectf = new RectangleF(LeftStatMargin, StatY + ((i*-1) * PixelEntreLesStats), RectangleNameWidth, RectangleNameHeight);
                DrawText(rectf, Stat, statTextFont, fontSize, canvas);
                rectf = DesignStatBars(canvas, i, StatBase);
                DrawText(rectf, "", statTextFont, fontSize, canvas);
            }
        }

        //Ajouter les barres de couleurs en dessous des stats
        public static RectangleF DesignStatBars(Graphics canvas, int i, int StatBase)
        {
            if (StatBase > 180)
            {
                rectf = new RectangleF(LeftStatMargin, RectangleStatY + ((i * -1) * PixelEntreLesStats), StatBase/2 , RectangleStatHeight);
                canvas.FillRectangle(Brushes.DarkMagenta, rectf);
            }
            else
            {
                rectf = new RectangleF(LeftStatMargin, RectangleStatY + ((i * -1) * PixelEntreLesStats), StatBase , RectangleStatHeight);
                canvas.FillRectangle(Brushes.Green, rectf);
            }
            return rectf;
        }

        private static void CreatePokemonNameText(string text, Graphics canvas)
        {
            fontSize = MeasureString(text, nameTextFont, canvas);
            //Placement du texte sur la carte
            rectf = new RectangleF(((CardSize.Width / 2) - (fontSize.Width / 2)), NameY, RectangleNameWidth, RectangleNameHeight);

            DrawText(rectf, text, nameTextFont, fontSize, canvas);
        }

        public static SizeF MeasureString(string text, Font font, Graphics canvas)
        {
            return canvas.MeasureString(text, font);
        }

        private static void DrawText(RectangleF rectf, string text, Font textFont, SizeF fontSize, Graphics canvas)
        {
            canvas.DrawString(text, textFont, Brushes.White, rectf);
        }

        static Color getColorFromRGBString(string RGBString)
        {
            int R = int.Parse(RGBString.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
            int G = int.Parse(RGBString.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
            int B = int.Parse(RGBString.Substring(4,2), System.Globalization.NumberStyles.HexNumber);

            return Color.FromArgb(R, G, B);
        }

        private static void GenerateImageFile(PokemonSpecies pkmnSpecies, Pokemon pkmn, Bitmap pkmnImage, string savePath)
        {
            if(!System.IO.Directory.Exists(savePath))
            {
                System.IO.Directory.CreateDirectory(savePath);
            }
            string pkmnFileName = pkmnSpecies.ID.ToString("D3") + "-" + pkmn.Name + ".png";
            pkmnImage.Save(savePath + pkmnFileName, System.Drawing.Imaging.ImageFormat.Png);
        }

        private static void DrawTypes(PokemonSpecies pkmnSpecies, Pokemon pkmn, Graphics canvas)
        {
            if(pkmn.Types.Length == 2)
            {
                Bitmap bmpType1 = (Bitmap)PokemonTypeIcons.ResourceManager.GetObject(pkmn.Types[1].Type.Name);
                Bitmap bmpType2 = (Bitmap)PokemonTypeIcons.ResourceManager.GetObject(pkmn.Types[0].Type.Name);

                canvas.DrawImage(bmpType1, new Point(TypePosition.X - 34, TypePosition.Y));
                canvas.DrawImage(bmpType2, new Point(TypePosition.X + 2, TypePosition.Y));
            }
            else
            {
                Bitmap bmpType1 = (Bitmap)PokemonTypeIcons.ResourceManager.GetObject(pkmn.Types[0].Type.Name);
                canvas.DrawImage(bmpType1, new Point(TypePosition.X-16,TypePosition.Y));
            }
        }

        private static void DrawImage(PokemonSpecies pkmnSpecies, Pokemon pkmn, Graphics canvas)
        {
            Image lImage;
            string fileName = @".\Assets\PokemonArtwork\" + pkmn.Name.Replace(pkmnSpecies.Name, pkmnSpecies.ID.ToString())+ ".png";
            if (System.IO.File.Exists(fileName))
            {
                lImage = Bitmap.FromFile(fileName);
            }
            else
            {
                fileName = @".\Assets\PokemonSprites\" + pkmn.Name.Replace(pkmnSpecies.Name, pkmnSpecies.ID.ToString()) + ".png";
                if (System.IO.File.Exists(fileName))
                {
                    lImage = Bitmap.FromFile(fileName);
                }
                else
                {
                    fileName = "";
                    if (System.IO.File.Exists(fileName))
                    {
                        lImage = Bitmap.FromFile(fileName);
                    }
                    else
                    {
                        lImage = new Bitmap(ImageRectangle.Width, ImageRectangle.Height);
                    }
                }
            }

            canvas.DrawImage(lImage, ImageRectangle);
        }

        private static void DrawBorders(PokemonSpecies pkmnSpecies, Pokemon pkmn, Graphics canvas)
        {
            Pen p = new Pen(Brushes.DarkSlateGray);
            p.Width = 2;
            p.Alignment = System.Drawing.Drawing2D.PenAlignment.Inset;

            canvas.DrawRectangle(p, CardBorderRectangle);

            canvas.DrawRectangle(p, ImageBorderRectangle);
        }

        private static void DrawBackground(PokemonSpecies pkmnSpecies, Pokemon pkmn, Graphics canvas)
        {
            Brush theBrush;
            if(pkmn.Types.Length == 2)
            {
                System.Drawing.Drawing2D.LinearGradientBrush lgb = new System.Drawing.Drawing2D.LinearGradientBrush(new Rectangle(new Point(0, 0), CardSize), PokemonTypeColors[pkmn.Types[1].Type.Name], PokemonTypeColors[pkmn.Types[0].Type.Name], 0.0f);
                /*lgb.Blend = new System.Drawing.Drawing2D.Blend();
                lgb.Blend.Factors = new float[] { 1.0f, 0.6f, 1.0f, 0.5f };
                lgb.Blend.Positions = new float[] { 0.0f, 0.45f, 0.55f, 1.0f };
                canvas.FillRectangle(lgb, new Rectangle(new Point(0, 0), CardSize));*/
                theBrush = lgb;
            }
            else
            {
                theBrush = new SolidBrush(PokemonTypeColors[pkmn.Types[0].Type.Name]);
            }
            canvas.FillRectangle(theBrush, new Rectangle(new Point(0, 0), CardSize));
        }
    }
}
